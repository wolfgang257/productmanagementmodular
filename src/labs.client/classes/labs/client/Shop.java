/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labs.client;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.ServiceLoader;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import labs.pm.data.Product;
import labs.pm.data.Rating;
import labs.pm.service.ProductManager;

/**
 * Proyecto para practicar con JAVA 11
 *
 * @author julia
 */
public class Shop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        AtomicInteger clientCount = new AtomicInteger(0);
        
        ResourceFormatter formatter = ResourceFormatter.getResourceFormatter("es-CO");

        //ProductManager pm = ProductFileManager.getInstance();
        ServiceLoader<ProductManager> serviceLoader = ServiceLoader.load(ProductManager.class);
        ProductManager pm = serviceLoader.findFirst().get();
        //pm.printProductReport(101, "es-CO");
        //pm.printProductReport(101, "en-GB");

        Callable<String> client = () -> {
            String clientId = "Client" + clientCount.incrementAndGet();
            String threadName = Thread.currentThread().getName();
            int productId = ThreadLocalRandom.current().nextInt(63) + 101;
            String languageTag = ResourceFormatter.getSupportedLocales()
                    .stream()
                    .skip(ThreadLocalRandom.current().nextInt(6))
                    .findFirst().get();

            StringBuilder log = new StringBuilder();
            log.append(clientId+" "+threadName+"\n-\tstart of log}\t-\n");
            log.append(pm.getDiscounts(languageTag) 
                    .entrySet().stream()
                    .map(entry -> entry.getKey()+"\t"+entry.getValue())
                    .collect(Collectors.joining("\n")) 
            );
            
            Product product = pm.reviewProduct(productId, Rating.FOUR_STAR, "Another review");
            log.append((product != null) 
                ? "\nProduct "+productId+" reviewed\n" : "\nProduct "+productId+" not reviewed\n");
            pm.printProductReport(productId, languageTag, clientId);
            log.append(clientId+" generated reprot for "+productId+" product");            
            log.append("\n-\tend of log}\t-\n");
            
            return log.toString();
        };
        
        List<Callable<String>> clients = Stream.generate(() -> client)
            .limit(5)
            .collect(Collectors.toList());
        
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            List<Future<String>> results = executorService.invokeAll(clients);
            executorService.shutdown();
            results.stream().forEach(result -> {
                try {
                    System.out.println(result.get());
                } catch (InterruptedException | ExecutionException ex) {
                    Logger.getLogger(Shop.class.getName()).log(Level.SEVERE, "Error retrieving client log", ex);
                } 
            });
        } catch (InterruptedException ex) {
            Logger.getLogger(Shop.class.getName()).log(Level.SEVERE, "Error invoking clients", ex);
        }
    }

    
    
    
    /**
     * @param args the command line arguments
     */
    /*public static void main(String[] args) {

        ProductManager pm = new ProductManager("es-CO");

        Product p1 = pm.createProduct(101, "Tea", BigDecimal.valueOf(1.99), Rating.NO_RATED);
        pm.printProductReport(101);
        pm.reviewProduct(p1, Rating.FOUR_STAR, "Nice cup");
        pm.reviewProduct(p1, Rating.THREE_STAR, "Middle rare");
        pm.reviewProduct(p1, Rating.FIVE_STAR, "Great");
//        pm.printProductReport(p1);

        pm.createProduct(103, "Cake", BigDecimal.valueOf(2.03), Rating.NO_RATED, LocalDate.now().plusDays(2));
//        pm.printProductReport(103);
        pm.reviewProduct(103, Rating.FOUR_STAR, "Nice cake");
        pm.reviewProduct(103, Rating.THREE_STAR, "Middle rare cake");
        pm.reviewProduct(103, Rating.FIVE_STAR, "Great eat");
        pm.printProductReport(103);

        pm.createProduct(104, "Cookie", BigDecimal.valueOf(2.03), Rating.NO_RATED, LocalDate.now().plusDays(2));
        pm.reviewProduct(104, Rating.TWO_STAR, "Nice Cookie");
        pm.reviewProduct(104, Rating.THREE_STAR, "Middle rare Cookie");
        pm.reviewProduct(104, Rating.ONE_STAR, "Great Cookie");
        
//        pm.parseReview("104,3,Nice hot cup of tea with 3");
//        pm.parseReview("104,4,Nice hot cup of tea with 4");
//        pm.parseReview("104,5,Nice hot cup of tea with 5");
        pm.printProductReport(104);
        
//        pm.parseProduct("D,105,Tea2,1.89,0,2019-09-18"); 
//        pm.printProductReport(105);
        
        //usando lambdas espressions
        Comparator<Product> ratingSorter = (pA, pB) -> pB.getRating().ordinal()-pA.getRating().ordinal();
        Comparator<Product> priceSorter = (pA, pB) -> pB.getPrice().compareTo(pA.getPrice());
        //pm.printProducts((pA, pB) -> pB.getRating().ordinal()-pA.getRating().ordinal());
        //pm.printProducts((pA, pB) -> pB.getPrice().compareTo(pA.getPrice()));
        
        pm.printProducts(
                p -> p.getPrice().floatValue() < 20,
                ratingSorter.thenComparing(priceSorter).reversed());
        
        pm.getDiscounts().forEach(
                (rating, discount) -> System.out.println(rating + "\t" +discount)
        );
        
//        pm.dumpData();
//        pm.restoreData();
//        System.out.println("After restore");
//         pm.printProductReport(104);

//        Product p2 = pm.createProduct(102, "Coffee", BigDecimal.valueOf(1.99), Rating.FOUR_STAR);
//        Product p3 = pm.createProduct(103, "Cake", BigDecimal.valueOf(3.99), Rating.FIVE_STAR, LocalDate.now().plusDays(2));
//        Product p4 = pm.createProduct(104, "Cookie", BigDecimal.valueOf(3.99), Rating.TWO_STAR, LocalDate.now());
//        Product p5 = p3.applyRating(Rating.THREE_STAR);
//        Product p8 = p4.applyRating(Rating.FIVE_STAR);
//        Product p9 = p1.applyRating(Rating.TWO_STAR);
//
//        System.out.println(p1);
//        System.out.println(p2);
//        System.out.println(p3);
//        System.out.println(p4);
//        System.out.println(p5);
//        System.out.println(p8);
//        System.out.println(p9);
//
//        Product p6 = pm.createProduct(106, "Chocolate", BigDecimal.valueOf(2.99), Rating.FIVE_STAR);
//        Product p7 = pm.createProduct(106, "Chocolate", BigDecimal.valueOf(2.99), Rating.FIVE_STAR, LocalDate.now().plusDays(2));
//        System.out.println(p6.equals(p7));
//        
//        System.out.println(p3.getBestBefore());
//        System.out.println(p1.getBestBefore());
//        String[] names = {"Mary", "Jane", "Elizabeth", "Jo"};
//        Arrays.sort(names, new Comparator<String>() {
//            public int compare(String s1, String s2) {
//                return s2.length() - s1.length();
//            }
//        });
//        for (String name : names) {
//            System.out.println(name);
//        }
//        String[] names = {"Mary", "Jane", "Ann", "Tom"};
//        Arrays.sort(names);
//        int x = Arrays.binarySearch(names, "Ann");
//        System.out.println(x);
    }*/
}
