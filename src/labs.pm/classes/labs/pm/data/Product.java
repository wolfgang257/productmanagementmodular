/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labs.pm.data;

import java.io.Serializable;
import java.math.BigDecimal;
import static java.math.RoundingMode.HALF_UP;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author julia
 */
public abstract class Product implements Rateable<Product>, Serializable{
    
    private int id;
    private String name;
    private BigDecimal price;
    private Rating rating;
    
    /**
     * Discount rate is 10%
     */
    private static final BigDecimal DISCOUNT_RATE=BigDecimal.valueOf(0.1); 

    /**
     * Constructor
     * @param id
     * @param name
     * @param price
     * @param rating 
     */
    public Product(int id, String name, BigDecimal price, Rating rating) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public Rating getRating() {
        return rating;
    }
    
    /**
     * Calculates discount based on product price and {@link DISCOUNT_RATE discount rate} 
     * @return  {@link java.math.BigDecimal BigDecimal} value of the discount
     */
    public BigDecimal getDiscount() {       
        return price.multiply(DISCOUNT_RATE).setScale(2, HALF_UP);
    }      
    
    /**
     * Assumes that the best before date is today
     * @return the current date
     */
    public LocalDate getBestBefore(){
        return LocalDate.now();
    }
    
    //public abstract Product applyRating(Rating newRating);

    @Override
    public String toString() {
        return "" + "id=" + id + ", name=" + name + ", price=" + price + ","+getDiscount()+", rating=" + rating.getStars() + " "+getBestBefore();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (this.id != other.id) {
            return false;
        }
//        if (!Objects.equals(this.name, other.name)) {
//            return false;
//        }
        return true;
    }
    
    
    
}
