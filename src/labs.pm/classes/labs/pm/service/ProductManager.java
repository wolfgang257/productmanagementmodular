/*
 * Copyright (C) 2021 juliandresog
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * ProductManager Versión 1.0 29/03/2021
 *
 * Copyright(c) 2007-2020, Boos IT.
 * admin@boos.com.co
 *
 * http://boos.cloud/license
 **/

package labs.pm.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import labs.pm.data.Product;
import labs.pm.data.Rating;

/**
 * La descripcion de la clase va aqui.
 * @version 	1.0 29/03/2021
 * @author juliandresog
 */
public interface ProductManager {   

    public Product createProduct(int id, String name, BigDecimal price, Rating rating, LocalDate bestBefore);
    public Product createProduct(int id, String name, BigDecimal price, Rating rating);
    public Product reviewProduct(int id, Rating rating, String comments);
    public void printProductReport(int id, String languageTag, String client);
    public Product findProduct(int id) throws ProductManagerException;
    public void printProducts(Predicate<Product> filter, Comparator<Product> sorter, String languageTag);
    public Map<String, String> getDiscounts(String languageTag);
    
}
